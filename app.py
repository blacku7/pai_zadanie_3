from flask import Flask, render_template

app = Flask(__name__,static_url_path='/static')

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/credentials")
def credentials():
    return render_template("credentials.html", author="Damian Wilamek")

@app.route("/Aboutme")
def aboutme():
    return render_template("Aboutme.html")

@app.route("/table")
def table():
    return render_template("table.html")

if __name__ == "__main__":

    app.run()

